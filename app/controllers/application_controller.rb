class ApplicationController < ActionController::Base
  
  
  protect_from_forgery
  
  
  def auth_user
    
    if !user_signed_in?
      flash[:notice] = " You need to be Logged in "
    end
    
    redirect_to "/" unless user_signed_in?
  end
  
  
  
end
