class HomeController < ApplicationController
  
  before_filter :auth_user, :except => [:survey_view]
  
  def survey_view
    
    if user_signed_in?
    
      if current_user.role == 100
        @allSurveys = SurveyRecord.where("status = ?","published").order("name")
      else
        @surveys = SurveyRecord.all(:order=>"name")
        redirect_to survey_records_path
      end
    
    else
       @allSurveys = SurveyRecord.where("status = ?","published").order("name")
    end
    
  end
  
  
  def survey_session
    
    url = params[:name]
        
    survey = SurveyRecord.where("url = ?",url)
    
    survey.each do |record|
    
      @survey_record = SurveyRecord.find(record.id)
    
    end  
    @submission = Submission.new
    
    respond_to do |format|
      if @survey_record.nil?
        format.html { redirect_to "/home/error_survey" }
        format.json { head :ok }
      else
        @submission = Submission.new
        format.html 
        format.json { head :ok }
      end
    end
  end
  
end
