class QuestionsController < ApplicationController
  
  load_and_authorize_resource
     before_filter :auth_user

  # GET /questions
  # GET /questions.json
  def index
    @questions = Question.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @questions }
    end
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
    @question = Question.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @question }
    end
  end

  # GET /questions/new
  # GET /questions/new.json
  def new
    
    @survey_record = SurveyRecord.find(params[:survey_id])
    @questions = @survey_record.questions
            
    if @questions[0].nil?
      record_order = 1
    else
      record_order = @questions[@questions.size()-1].record_order + 1
    end
    
    @question = Question.new
    @question.survey_record_id = @survey_record.id
    @question.record_order = record_order 
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @question }
      format.js
    end
  end

  # GET /questions/1/edit
  def edit
    @question = Question.find(params[:id])
    @questions = @question.survey_record.questions
    
    @button_text = " Update Question"
    
    respond_to do |format|
      format.js
    end
    
  end

  # POST /questions
  # POST /questions.json
  def create
    
    @newQuestion = Question.new(params[:question])
    
    survey = SurveyRecord.find(params[:question][:survey_record_id])
    
    @questions = survey.questions
    
    @button_text = " Create Question"
    
    respond_to do |format|
      if @newQuestion.save
        
        @question = Question.new
        @question.survey_record_id = @newQuestion.survey_record_id
        
        if @questions[0].nil?
          record_order = 1
        else
          record_order = @questions[@questions.size()-1].record_order + 1
        end
        
        @question.record_order = record_order
        @success = "Question Saved successfully "
        format.html { redirect_to @question, notice: 'Question was successfully created.' }
        format.json { render json: @question, status: :created, location: @question }
        format.js
      else
        @error = " Error in saving Question "
        format.html { render action: "new" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PUT /questions/1
  # PUT /questions/1.json
  def update
    @updateQuestion = Question.find(params[:id])
    @questions = @question.survey_record.questions
    
    @question = Question.new
    @question.survey_record_id = @updateQuestion.survey_record_id
    
    if @questions[0].nil?
          record_order = 1
        else
          record_order = @questions[@questions.size()-1].record_order + 1
        end
        
    @question.record_order = record_order
    @success = "Question Updated successfully "

    respond_to do |format|
      if @updateQuestion.update_attributes(params[:question])
        format.html { redirect_to @updateQuestion, notice: 'Question was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @updateQuestion.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @deleteQ = Question.find(params[:id])
    
    quesnName = @deleteQ.name
    @survey_record = @deleteQ.survey_record
    
    @deleteQ.destroy
    
    @questions = @survey_record.questions
    @question = Question.new
    @question.survey_record_id = @survey_record.id
        
    if @questions[0].nil?
          record_order = 1
        else
          record_order = @questions[@questions.size()-1].record_order + 1
    end
    
    @question.record_order = record_order
    
    @success = "Question"+quesnName.to_s+"has been destroyed"

    respond_to do |format|
      format.html { redirect_to questions_url }
      format.json { head :no_content }
      format.js
    end
  end
end
