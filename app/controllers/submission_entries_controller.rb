class SubmissionEntriesController < ApplicationController
  
  load_and_authorize_resource
     before_filter :auth_user

  
  # GET /submission_entries
  # GET /submission_entries.json
  def index
    @submission_entries = SubmissionEntry.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @submission_entries }
    end
  end

  # GET /submission_entries/1
  # GET /submission_entries/1.json
  def show
    @submission_entry = SubmissionEntry.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @submission_entry }
    end
  end

  # GET /submission_entries/new
  # GET /submission_entries/new.json
  def new
    @submission_entry = SubmissionEntry.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @submission_entry }
    end
  end

  # GET /submission_entries/1/edit
  def edit
    @submission_entry = SubmissionEntry.find(params[:id])
  end

  # POST /submission_entries
  # POST /submission_entries.json
  def create
    @submission_entry = SubmissionEntry.new(params[:submission_entry])

    respond_to do |format|
      if @submission_entry.save
        format.html { redirect_to @submission_entry, notice: 'Submission entry was successfully created.' }
        format.json { render json: @submission_entry, status: :created, location: @submission_entry }
      else
        format.html { render action: "new" }
        format.json { render json: @submission_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /submission_entries/1
  # PUT /submission_entries/1.json
  def update
    @submission_entry = SubmissionEntry.find(params[:id])

    respond_to do |format|
      if @submission_entry.update_attributes(params[:submission_entry])
        format.html { redirect_to @submission_entry, notice: 'Submission entry was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @submission_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /submission_entries/1
  # DELETE /submission_entries/1.json
  def destroy
    @submission_entry = SubmissionEntry.find(params[:id])
    @submission_entry.destroy

    respond_to do |format|
      format.html { redirect_to submission_entries_url }
      format.json { head :no_content }
    end
  end
end
