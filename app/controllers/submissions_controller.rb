class SubmissionsController < ApplicationController
  
  load_and_authorize_resource
     before_filter :auth_user

  
  # GET /submissions
  # GET /submissions.json
  def index
    
    survey_id = params[:survey_id]
    @survey_record = SurveyRecord.find(survey_id)
    
    @submissions = @survey_record.submissions.paginate(:page => params[:page],
      :per_page => 10)
    
    @sortedQuestions = @survey_record.questions.sort!{ |a,b| a.record_order <=> b.record_order }
    
    @ques_ans_map = Hash.new
    
    @submissions.each do |submission|
      
         submissionEntries = submission.submission_entries
          
         entryMap = Hash.new   
         
          @sortedQuestions.each do |question|
            
            entryMap[question.name] = ""
            
            submissionEntries.each do |entry|
              
              if entry.question.name == question.name
                
                entryMap[question.name] = entry.answer_value
                break
                
              end
            end
                        
          end  
                
      @ques_ans_map[submission.id] = entryMap
           
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @submissions }
    end
  end

  # GET /submissions/1
  # GET /submissions/1.json
  def show
    @submission = Submission.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @submission }
    end
  end

  # GET /submissions/new
  # GET /submissions/new.json
  def new
    @submission = Submission.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @submission }
    end
  end

  # GET /submissions/1/edit
  def edit
    @submission = Submission.find(params[:id])
  end

  # POST /submissions
  # POST /submissions.json
  def create
    @submission = Submission.new()
    @submission.survey_record_id = params[:survey_record_id]
    @submission.user_id = current_user.id
  
    respond_to do |format|
      
      if @submission.save
        
        @allAnswers = params[:question]  
        
        totalMarks = 0
         
        @allAnswers.each do |qKey,aVal|
            
                @submissionEntry = SubmissionEntry.create!(
                                        :submission_id => @submission.id ,
                                        :question_id => qKey , 
                                        :answer_value => aVal)
                                      
                @submissionEntry.save
    
         end
        
        survey = SurveyRecord.find(params[:survey_record_id])
        
        @survey_message = survey.message
        
        format.html { redirect_to submission_path({:id =>@submission.id,:successMessage => survey.message}),
          notice: 'Your Opinion has been recorded sucessfully' }
        format.json { render json: @submission, status: :created, location: @question }
      else
        format.html { render action: "new" }
        format.json { render json: @submission.errors, status: :unprocessable_entity }
      end
    end   
  end

  # PUT /submissions/1
  # PUT /submissions/1.json
  def update
    @submission = Submission.find(params[:id])

    respond_to do |format|
      if @submission.update_attributes(params[:submission])
        format.html { redirect_to @submission, notice: 'Submission was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /submissions/1
  # DELETE /submissions/1.json
  def destroy
    @submission = Submission.find(params[:id])
    
    survey_id = @submission.survey_record_id
    
    @submission.destroy

    respond_to do |format|
      format.html { redirect_to submissions_url( :survey_id => survey_id )}
      format.json { head :no_content }
    end
  end
end
