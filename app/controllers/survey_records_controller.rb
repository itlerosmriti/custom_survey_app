class SurveyRecordsController < ApplicationController
  
  # GET /survey_records
  # GET /survey_records.json
  before_filter :auth_user, :except => [:index]
  load_and_authorize_resource 
   
  def index
    
    @survey_records = SurveyRecord.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @survey_records }
    end
  end

  # GET /survey_records/1
  # GET /survey_records/1.json
  def show
    @survey_record = SurveyRecord.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @survey_record }
    end
  end

  # GET /survey_records/new
  # GET /survey_records/new.json
  def new
    @survey_record = SurveyRecord.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @survey_record }
    end
  end

  # GET /survey_records/1/edit
  def edit
    
    @survey_record = SurveyRecord.find(params[:id])
    
    @questions = @survey_record.questions.order("record_order asc")
        
    if @questions[0].nil?
      record_order = 1
    else
      record_order = @questions[@questions.size()-1].record_order + 1
    end
    
    @question = @survey_record.questions.new
    @question.record_order = record_order 
    
  end

  # POST /survey_records
  # POST /survey_records.json
  def create
    @survey_record = SurveyRecord.new(params[:survey_record])
    
    @questions = @survey_record.questions.order("record_order asc")
    
    if @questions[0].nil?
      record_order = 1
    else
      record_order = @questions[@questions.size()-1].record_order + 1
    end
    
    if @survey_record.url.nil? || @survey_record.url == ""
      url = @survey_record.name
      url[" "] = "_"
      @survey_record.url = @survey_record.name
    end
    
    respond_to do |format|
      if @survey_record.save
        
        @question = @survey_record.questions.new
        @question.record_order = record_order 
        
        format.html { render action: "edit", notice: 'Survey record was successfully created.' }
        format.json { render json: @survey_record, status: :created, location: @survey_record }
        format.js
      else
        puts @survey_record.errors.full_messages
        format.html { render action: "new" }
        format.json { render json: @survey_record.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PUT /survey_records/1
  # PUT /survey_records/1.json
  def update
    @survey_record = SurveyRecord.find(params[:id])
    
    @questions = @survey_record.questions.order("record_order asc");
    @question = Question.new
    @question.survey_record_id = @survey_record.id
    
    if @questions[0].nil?
      record_order = 1
    else
      record_order = @questions[@questions.size()-1].record_order + 1
    end
    
    @question.record_order = record_order

    respond_to do |format|
      if @survey_record.update_attributes(params[:survey_record])
        @success = "Survey Record Successfully updated"
        format.html { redirect_to @survey_record, notice: 'Survey record was successfully updated.' }
        format.json { head :no_content }
        format.js 
      else
        errorMsg = ""
        
        @survey_record.errors.full_messages.each do |key, value| 
               errorMsg = errorMsg+key.to_s+"--"+value.to_s   
        end          
        
        @error = errorMsg
        format.html { render action: "edit" }
        format.json { render json: @survey_record.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /survey_records/1
  # DELETE /survey_records/1.json
  def destroy
    @survey_record = SurveyRecord.find(params[:id])
    @survey_record.destroy 
    
    respond_to do |format|
      format.html { redirect_to survey_records_url ,notice: " Survey Record successfully deleted"}
      format.json { head :no_content }
    end
  end
end
