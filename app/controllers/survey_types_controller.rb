class SurveyTypesController < ApplicationController
  
  load_and_authorize_resource
     before_filter :auth_user

  
  # GET /survey_types
  # GET /survey_types.json
  def index
    @survey_types = SurveyType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @survey_types }
    end
  end

  # GET /survey_types/1
  # GET /survey_types/1.json
  def show
    @survey_type = SurveyType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @survey_type }
    end
  end

  # GET /survey_types/new
  # GET /survey_types/new.json
  def new
    @survey_type = SurveyType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @survey_type }
    end
  end

  # GET /survey_types/1/edit
  def edit
    @survey_type = SurveyType.find(params[:id])
  end

  # POST /survey_types
  # POST /survey_types.json
  def create
    @survey_type = SurveyType.new(params[:survey_type])

    respond_to do |format|
      if @survey_type.save
        format.html { redirect_to @survey_type, notice: 'Survey type was successfully created.' }
        format.json { render json: @survey_type, status: :created, location: @survey_type }
      else
        format.html { render action: "new" }
        format.json { render json: @survey_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /survey_types/1
  # PUT /survey_types/1.json
  def update
    @survey_type = SurveyType.find(params[:id])

    respond_to do |format|
      if @survey_type.update_attributes(params[:survey_type])
        format.html { redirect_to @survey_type, notice: 'Survey type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @survey_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /survey_types/1
  # DELETE /survey_types/1.json
  def destroy
    @survey_type = SurveyType.find(params[:id])
    @survey_type.destroy

    respond_to do |format|
      format.html { redirect_to survey_types_url }
      format.json { head :no_content }
    end
  end
end
