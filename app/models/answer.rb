class Answer < ActiveRecord::Base
  attr_accessible :marks, :name, :qusestion_id, :record_order
  
  validates :name,:presence => true
  validates :record_order, :presence=>true
  
  validates_with AnswerValidator

  
end
