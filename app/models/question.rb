class Question < ActiveRecord::Base
  attr_accessible :id, :name, :record_order, :survey_record_id, :survey_type_id
  belongs_to :survey_record
  belongs_to :survey_type
  has_many :submission_entries, :dependent => :delete_all
  
  validates :survey_type_id , :presence =>true
  validates :name , :presence => true
  validates :record_order, :presence => true
  
  validates_with QuestionValidator
end
