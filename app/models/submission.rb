class Submission < ActiveRecord::Base
  attr_accessible :attempt_time, :survey_record_id, :user_id
  
  validates :survey_record_id, :presence => true
  validates :user_id, :presence => true

  belongs_to :survey_record
  belongs_to :user
  
  has_many :submission_entries , :dependent => :delete_all
end
