class SubmissionEntry < ActiveRecord::Base

  attr_accessible :answer_value, :question_id, :submission_id
  
  validates :answer_value, :presence => true
  validates :question_id, :presence => true 
  validates :submission_id, :presence => true
  
  belongs_to :submission
  belongs_to :question
end
