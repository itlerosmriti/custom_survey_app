class SurveyRecord < ActiveRecord::Base
  attr_accessible :description, :message, :name, :url,:status
  
  STATUS_UNPUB     = "unpublished"
  STATUS_PUBLISHED   = "published"
  
  has_many :questions,:dependent => :delete_all
  has_many :submissions,:dependent => :delete_all
  
  validates :name ,:uniqueness => true,  :presence =>true
  validates :url, :uniqueness => true, :presence => true
  validates :status , :presence=>true
  validates_format_of :url, :with => /^[a-zA-Z\d]*$/i,
        :message => "can only contain letters and numbers."
      
  validates :message, :presence => true
  validates :description , :presence => true
  
  def self.getStatusMap
    status_hash = Hash.new
    status_hash["unpublished"] = "unpublished"
    status_hash["published"] = "published"
    
    status_hash
  end
  
end
