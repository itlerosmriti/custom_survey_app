class SurveyType < ActiveRecord::Base
  attr_accessible :need_option, :type_data, :type_for, :type_name
 
  validates :type_data, :presence => true
  validates :type_for, :presence => true
  validates :type_name, :presence => true
  def self.getTypeMap
    allType = SurveyType.all
    typeMap = Hash.new
    
    typeMap["Select Type"] = ""
    
    allType.each do |type|
      typeMap[type.type_name] = type.id
    end
    
    typeMap
  end
  
end
