# To change this template, choose Tools | Templates
# and open the template in the editor.

class AnswerValidator < ActiveModel::Validator
  
  def validate(record)
    
    order = record.record_order
    
    survey_id = record.question_id
    
    order_exists = nil
    
    if !record.new_record?
    
      order_exists = Answer.where("question_id = ? and record_order = ? and id != ?",survey_id,order,record.id)  
    
    else
      
       order_exists = Answer.where("question_id = ? and record_order = ?",survey_id,order) 
      
    end
    
    if !(order_exists.empty?)  
       
      record.errors.add(:order,'Answer on this order already exists , choose other order')

    end
        
  end
end
