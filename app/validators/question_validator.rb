# To change this template, choose Tools | Templates
# and open the template in the editor.

class QuestionValidator < ActiveModel::Validator
  
  def validate(record)
    
    order = record.record_order
    
    survey_id = record.survey_record_id
    
    order_exists = nil
    
    if !record.new_record?
    
      order_exists = Question.where("survey_record_id = ? and record_order = ? and id != ?",survey_id,order,record.id)  
    
    else
      
       order_exists = Question.where("survey_record_id = ? and record_order = ?",survey_id,order)  

      
    end  

    if !(order_exists.empty?)  
       
      record.errors.add(:order,'Question on this order already exists , choose other order')

    end
    
  end

end
