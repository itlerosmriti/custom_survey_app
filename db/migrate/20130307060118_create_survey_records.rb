class CreateSurveyRecords < ActiveRecord::Migration
  def change
    create_table :survey_records do |t|
      t.string :name
      t.text :description
      t.string :url
      t.text :message

      t.timestamps
    end
  end
end
