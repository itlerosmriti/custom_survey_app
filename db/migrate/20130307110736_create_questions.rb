class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :name
      t.integer :survey_record_id
      t.integer :record_order
      t.integer :type_id

      t.timestamps
    end
  end
end
