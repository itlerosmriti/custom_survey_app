class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.text :name
      t.integer :question_id
      t.integer :record_order
      t.integer :marks

      t.timestamps
    end
  end
end
