class AddRoleToUser < ActiveRecord::Migration
  def change
        add_column :users, :role, :integer, :default => ::User::Role::USER
  end
end
