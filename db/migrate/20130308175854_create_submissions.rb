class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.integer :survey_record_id
      t.integer :user_id
      t.integer :attempt_time

      t.timestamps
    end
  end
end
