class CreateSubmissionEntries < ActiveRecord::Migration
  def change
    create_table :submission_entries do |t|
      t.integer :submission_id
      t.integer :questions_id
      t.text :answer_value

      t.timestamps
    end
  end
end
