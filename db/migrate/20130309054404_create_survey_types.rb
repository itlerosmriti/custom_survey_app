class CreateSurveyTypes < ActiveRecord::Migration
  def change
    create_table :survey_types do |t|
      t.string :type_name
      t.boolean :need_option
      t.string :type_for
      t.string :type_data

      t.timestamps
    end
  end
end
