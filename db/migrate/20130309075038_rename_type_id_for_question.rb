class RenameTypeIdForQuestion < ActiveRecord::Migration
  def up
    
    rename_column :questions , :type_id , :survey_type_id
    
  end

  def down
  end
end
