class RenameQuestionIdSubmissionEntry < ActiveRecord::Migration
  def up
        rename_column :submission_entries , :questions_id , :question_id

  end

  def down
  end
end
