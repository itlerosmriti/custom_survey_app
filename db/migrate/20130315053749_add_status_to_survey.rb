class AddStatusToSurvey < ActiveRecord::Migration
  def change
    add_column :survey_records, :status , :string , :default=>"UNPUB"
  end
end
