# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
types = SurveyType.create! ([
    {
      :type_name => "essay",
      :need_option => 0,
      :type_for => "question",
      :type_data => "text"
    },
    {
      :type_name => "numeric",
      :need_option => 0,
      :type_for => "question",
      :type_data => "number"
    },
    {
      :type_name => "date",
      :need_option => 0,
      :type_for => "question",
      :type_data => "date"
    },
    {
      :type_name => "radio",
      :need_option => 1,
      :type_for => "question",
      :type_data => "text"
    }
])
