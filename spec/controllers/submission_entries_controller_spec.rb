require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe SubmissionEntriesController do

  # This should return the minimal set of attributes required to create a valid
  # SubmissionEntry. As you add validations to SubmissionEntry, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    {}
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # SubmissionEntriesController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all submission_entries as @submission_entries" do
      submission_entry = SubmissionEntry.create! valid_attributes
      get :index, {}, valid_session
      assigns(:submission_entries).should eq([submission_entry])
    end
  end

  describe "GET show" do
    it "assigns the requested submission_entry as @submission_entry" do
      submission_entry = SubmissionEntry.create! valid_attributes
      get :show, {:id => submission_entry.to_param}, valid_session
      assigns(:submission_entry).should eq(submission_entry)
    end
  end

  describe "GET new" do
    it "assigns a new submission_entry as @submission_entry" do
      get :new, {}, valid_session
      assigns(:submission_entry).should be_a_new(SubmissionEntry)
    end
  end

  describe "GET edit" do
    it "assigns the requested submission_entry as @submission_entry" do
      submission_entry = SubmissionEntry.create! valid_attributes
      get :edit, {:id => submission_entry.to_param}, valid_session
      assigns(:submission_entry).should eq(submission_entry)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new SubmissionEntry" do
        expect {
          post :create, {:submission_entry => valid_attributes}, valid_session
        }.to change(SubmissionEntry, :count).by(1)
      end

      it "assigns a newly created submission_entry as @submission_entry" do
        post :create, {:submission_entry => valid_attributes}, valid_session
        assigns(:submission_entry).should be_a(SubmissionEntry)
        assigns(:submission_entry).should be_persisted
      end

      it "redirects to the created submission_entry" do
        post :create, {:submission_entry => valid_attributes}, valid_session
        response.should redirect_to(SubmissionEntry.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved submission_entry as @submission_entry" do
        # Trigger the behavior that occurs when invalid params are submitted
        SubmissionEntry.any_instance.stub(:save).and_return(false)
        post :create, {:submission_entry => {}}, valid_session
        assigns(:submission_entry).should be_a_new(SubmissionEntry)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        SubmissionEntry.any_instance.stub(:save).and_return(false)
        post :create, {:submission_entry => {}}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested submission_entry" do
        submission_entry = SubmissionEntry.create! valid_attributes
        # Assuming there are no other submission_entries in the database, this
        # specifies that the SubmissionEntry created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        SubmissionEntry.any_instance.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, {:id => submission_entry.to_param, :submission_entry => {'these' => 'params'}}, valid_session
      end

      it "assigns the requested submission_entry as @submission_entry" do
        submission_entry = SubmissionEntry.create! valid_attributes
        put :update, {:id => submission_entry.to_param, :submission_entry => valid_attributes}, valid_session
        assigns(:submission_entry).should eq(submission_entry)
      end

      it "redirects to the submission_entry" do
        submission_entry = SubmissionEntry.create! valid_attributes
        put :update, {:id => submission_entry.to_param, :submission_entry => valid_attributes}, valid_session
        response.should redirect_to(submission_entry)
      end
    end

    describe "with invalid params" do
      it "assigns the submission_entry as @submission_entry" do
        submission_entry = SubmissionEntry.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        SubmissionEntry.any_instance.stub(:save).and_return(false)
        put :update, {:id => submission_entry.to_param, :submission_entry => {}}, valid_session
        assigns(:submission_entry).should eq(submission_entry)
      end

      it "re-renders the 'edit' template" do
        submission_entry = SubmissionEntry.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        SubmissionEntry.any_instance.stub(:save).and_return(false)
        put :update, {:id => submission_entry.to_param, :submission_entry => {}}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested submission_entry" do
      submission_entry = SubmissionEntry.create! valid_attributes
      expect {
        delete :destroy, {:id => submission_entry.to_param}, valid_session
      }.to change(SubmissionEntry, :count).by(-1)
    end

    it "redirects to the submission_entries list" do
      submission_entry = SubmissionEntry.create! valid_attributes
      delete :destroy, {:id => submission_entry.to_param}, valid_session
      response.should redirect_to(submission_entries_url)
    end
  end

end
