require 'factory_girl'

FactoryGirl.define do
  factory :survey_record do |f|
    f.sequence(:name) { |n| "survey_test#{n}" }
    f.description "ABCD"
    f.sequence(:url) {|n| "survey_test_url#{n}"}
    f.message "This is a test message"
#    f.question ||= Factory.build(:question, :survey_record => f)
  end
 
 factory :question do |f|
   f.name "Question name"
   f.survey_record_id  1
   f.sequence(:record_order) {|n| n}
   f.survey_type_id 1
 end 
 
 factory :answer do |answer|
   answer.name "Test Answer"
   answer.record_order 1
   answer.marks 10
   answer.question_id 1
 end 

  factory :submission do |submission|
    submission.survey_record_id 1
    submission.user_id 1
    submission.attempt_time 1
  end 

  factory :submission_entry do |submission_entry| 
    submission_entry.answer_value 1
    submission_entry.question_id 1
    submission_entry.submission_id 1
  end
  
  factory :survey_type do |survey_types|
    survey_types.need_option 1
    survey_types.type_data "Date"
    survey_types.type_for "Question"
    survey_types.type_name "Types"
  end
end
