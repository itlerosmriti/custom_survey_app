require 'spec_helper'

describe "Answers" do
  
  
  let(:answer) {FactoryGirl.build(:answer)}
  
  subject{answer}
  
  describe "validations" do
    #context "for answer_value" do
     # it { should validate_presence_of(:answer_value)}
    #end
    
    context "for record_order" do
      it { should validate_presence_of(:record_order)}
      it  "should validate uniqueness of record_order for survey_record" do
        FactoryGirl.build(:answer, question_id: 1,record_order: 1)
        
        FactoryGirl.build(:answer,question_id: 1,record_order: 1).should be_valid
      end
    end
  end
  
  describe "GET /answers" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get answers_path
      response.status.should be(200)
    end
  end
end
