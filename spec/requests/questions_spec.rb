require 'spec_helper'

describe "Questions" do
  
  let(:question) {FactoryGirl.build(:question)}
  
  subject{question}
  
  describe 'validations' do

    context "for name" do
      it { should validate_presence_of(:name)}
      
    end
    
    context "record_order" do
      it { should validate_presence_of(:record_order)}
      it  "should validate uniqueness of record_order for survey_record" do
        FactoryGirl.build(:question,survey_record_id: 1,record_order: 1)
        FactoryGirl.build(:question,survey_record_id: 1,record_order: 1).should be_valid
      end
    end
  end

  
  describe "GET /questions" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get questions_path
      response.status.should be(200)
    end
  end
end
