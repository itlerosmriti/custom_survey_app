require 'spec_helper'

describe "Submission_entry" do
  
  let(:submission_entry) {FactoryGirl.build(:submission_entry)}
  
  subject{submission_entry}
  
  describe 'validations' do

     context "answer_value" do
      it { should validate_presence_of(:answer_value)}
       it { should_not allow_value("").for(:answer_value) }
     end
     
     context "questions_id" do
      it { should validate_presence_of(:questions_id)}
    it { should_not allow_value("").for(:questions_id) }
    end
    
     context "submission_id" do
      it { should validate_presence_of(:submission_id)}
       it { should_not allow_value("").for(:submission_id) }  
 
      end
    end
  end

  describe "GET /submission_entries" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get submission_entries_path
      response.status.should be(200)
    end
  end
end
