require 'spec_helper'

describe "Submission" do
  
  let(:submission) {FactoryGirl.build(:submission)}
  
  subject{submission}

   describe 'validations' do
    context "survey_record_id" do
      it { should validate_presence_of(:survey_record_id) }
      it { should_not allow_value("").for(:survey_record_id) }
       
    end
    
    context "for user_id " do
      it { should validate_presence_of(:user_id)}
      it { should_not allow_value(" ").for(:user_id)}
    end
 end
end


  describe "GET /submissions" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get submissions_path
      response.status.should be(200)
    end
  end
end
