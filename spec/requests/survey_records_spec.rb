require 'spec_helper'

describe "SurveyRecords" do
  
  let(:survey_record) {FactoryGirl.build(:survey_record)}
  
  subject{survey_record}
  
  describe 'validations' do
    context "for name" do
      it { should validate_presence_of(:name) }
      it { should_not allow_value(" ").for(:name) }
      it { should validate_uniqueness_of(:name)}
      
    end
    
    context "for description" do
      it { should validate_presence_of(:description)}
      it { should_not allow_value(" ").for(:description)}
    end
    
    context "for message" do
      it { should validate_presence_of(:message)}
      it { should_not allow_value(" ").for(:message)}

    end
    
    context "for url" do
      it {should validate_presence_of(:url)}
      it { should_not allow_value(" ").for(:url)}
      it { should validate_uniqueness_of(:url)}
    end
    
  end
  
    

  
  describe "GET /survey_records" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
#      get survey_records_index_path
#      response.status.should be(200)
    end
#    
#    it "should have the content 'Surveys For You All Surveys'" do
#      visit '/survey_records'
#      page.should have_content('Surveys For You All Surveys')
#    end 
  end
  
   describe "GET /survey_records/new" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
#      get survey_records_index_path
#      response.status.should be(200)
    end
    
    it "should have the content 'Create New Survey'" do
      visit '/survey_records/new'
      page.should have_content('Create New Survey')
    end 
  end
  
end
