require 'spec_helper'

describe "Survey_types" do
  
  let(:survey_type) {FactoryGirl.build(:survey_type)}
  
  subject{survey_type}
  
  describe 'validations' do
       
    
    context "for type_data " do
      it { should validate_presence_of(:type_data)}
      it { should_not allow_value("").for(:type_data)}
    end
    
    context "for type_for" do
      it { should validate_presence_of(:type_for)}
      it { should_not allow_value("").for(:type_for)}

    end
    
    context "for type_name" do
      it {should validate_presence_of(:type_name)}
      it { should_not allow_value("").for(:type_name)}
 
    end
    
  end
end
describe "SurveyTypes" do
  describe "GET /survey_types" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get survey_types_path
      response.status.should be(200)
    end
  end
end
