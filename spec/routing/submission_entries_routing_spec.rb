require "spec_helper"

describe SubmissionEntriesController do
  describe "routing" do

    it "routes to #index" do
      get("/submission_entries").should route_to("submission_entries#index")
    end

    it "routes to #new" do
      get("/submission_entries/new").should route_to("submission_entries#new")
    end

    it "routes to #show" do
      get("/submission_entries/1").should route_to("submission_entries#show", :id => "1")
    end

    it "routes to #edit" do
      get("/submission_entries/1/edit").should route_to("submission_entries#edit", :id => "1")
    end

    it "routes to #create" do
      post("/submission_entries").should route_to("submission_entries#create")
    end

    it "routes to #update" do
      put("/submission_entries/1").should route_to("submission_entries#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/submission_entries/1").should route_to("submission_entries#destroy", :id => "1")
    end

  end
end
