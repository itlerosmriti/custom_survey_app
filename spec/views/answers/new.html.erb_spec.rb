require 'spec_helper'

describe "answers/new" do
  before(:each) do
    assign(:answer, stub_model(Answer,
      :name => "MyText",
      :qusestion_id => 1,
      :record_order => 1,
      :marks => 1
    ).as_new_record)
  end

  it "renders new answer form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => answers_path, :method => "post" do
      assert_select "textarea#answer_name", :name => "answer[name]"
      assert_select "input#answer_qusestion_id", :name => "answer[qusestion_id]"
      assert_select "input#answer_record_order", :name => "answer[record_order]"
      assert_select "input#answer_marks", :name => "answer[marks]"
    end
  end
end
