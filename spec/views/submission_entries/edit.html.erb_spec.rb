require 'spec_helper'

describe "submission_entries/edit" do
  before(:each) do
    @submission_entry = assign(:submission_entry, stub_model(SubmissionEntry,
      :submission_id => 1,
      :questions_id => 1,
      :answer_value => "MyText"
    ))
  end

  it "renders the edit submission_entry form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => submission_entries_path(@submission_entry), :method => "post" do
      assert_select "input#submission_entry_submission_id", :name => "submission_entry[submission_id]"
      assert_select "input#submission_entry_questions_id", :name => "submission_entry[questions_id]"
      assert_select "textarea#submission_entry_answer_value", :name => "submission_entry[answer_value]"
    end
  end
end
