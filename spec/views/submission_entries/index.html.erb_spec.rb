require 'spec_helper'

describe "submission_entries/index" do
  before(:each) do
    assign(:submission_entries, [
      stub_model(SubmissionEntry,
        :submission_id => 1,
        :questions_id => 2,
        :answer_value => "MyText"
      ),
      stub_model(SubmissionEntry,
        :submission_id => 1,
        :questions_id => 2,
        :answer_value => "MyText"
      )
    ])
  end

  it "renders a list of submission_entries" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
