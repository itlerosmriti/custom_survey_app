require 'spec_helper'

describe "submission_entries/show" do
  before(:each) do
    @submission_entry = assign(:submission_entry, stub_model(SubmissionEntry,
      :submission_id => 1,
      :questions_id => 2,
      :answer_value => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/MyText/)
  end
end
