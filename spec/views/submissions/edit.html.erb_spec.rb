require 'spec_helper'

describe "submissions/edit" do
  before(:each) do
    @submission = assign(:submission, stub_model(Submission,
      :survey_record_id => 1,
      :user_id => 1,
      :attempt_time => 1
    ))
  end

  it "renders the edit submission form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => submissions_path(@submission), :method => "post" do
      assert_select "input#submission_survey_record_id", :name => "submission[survey_record_id]"
      assert_select "input#submission_user_id", :name => "submission[user_id]"
      assert_select "input#submission_attempt_time", :name => "submission[attempt_time]"
    end
  end
end
