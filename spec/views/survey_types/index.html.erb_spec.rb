require 'spec_helper'

describe "survey_types/index" do
  before(:each) do
    assign(:survey_types, [
      stub_model(SurveyType,
        :type_name => "Type Name",
        :need_option => false,
        :type_for => "Type For",
        :type_data => "Type Data"
      ),
      stub_model(SurveyType,
        :type_name => "Type Name",
        :need_option => false,
        :type_for => "Type For",
        :type_data => "Type Data"
      )
    ])
  end

  it "renders a list of survey_types" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Type Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Type For".to_s, :count => 2
    assert_select "tr>td", :text => "Type Data".to_s, :count => 2
  end
end
