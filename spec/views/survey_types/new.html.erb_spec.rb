require 'spec_helper'

describe "survey_types/new" do
  before(:each) do
    assign(:survey_type, stub_model(SurveyType,
      :type_name => "MyString",
      :need_option => false,
      :type_for => "MyString",
      :type_data => "MyString"
    ).as_new_record)
  end

  it "renders new survey_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => survey_types_path, :method => "post" do
      assert_select "input#survey_type_type_name", :name => "survey_type[type_name]"
      assert_select "input#survey_type_need_option", :name => "survey_type[need_option]"
      assert_select "input#survey_type_type_for", :name => "survey_type[type_for]"
      assert_select "input#survey_type_type_data", :name => "survey_type[type_data]"
    end
  end
end
