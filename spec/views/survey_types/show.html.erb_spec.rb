require 'spec_helper'

describe "survey_types/show" do
  before(:each) do
    @survey_type = assign(:survey_type, stub_model(SurveyType,
      :type_name => "Type Name",
      :need_option => false,
      :type_for => "Type For",
      :type_data => "Type Data"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Type Name/)
    rendered.should match(/false/)
    rendered.should match(/Type For/)
    rendered.should match(/Type Data/)
  end
end
