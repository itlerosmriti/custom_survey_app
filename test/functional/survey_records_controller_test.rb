require 'test_helper'

class SurveyRecordsControllerTest < ActionController::TestCase
  setup do
    @survey_record = survey_records(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:survey_records)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create survey_record" do
    assert_difference('SurveyRecord.count') do
      post :create, survey_record: { description: @survey_record.description, message: @survey_record.message, name: @survey_record.name, url: @survey_record.url }
    end

    assert_redirected_to survey_record_path(assigns(:survey_record))
  end

  test "should show survey_record" do
    get :show, id: @survey_record
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @survey_record
    assert_response :success
  end

  test "should update survey_record" do
    put :update, id: @survey_record, survey_record: { description: @survey_record.description, message: @survey_record.message, name: @survey_record.name, url: @survey_record.url }
    assert_redirected_to survey_record_path(assigns(:survey_record))
  end

  test "should destroy survey_record" do
    assert_difference('SurveyRecord.count', -1) do
      delete :destroy, id: @survey_record
    end

    assert_redirected_to survey_records_path
  end
end
