require 'test_helper'

class QuestionTest < ActiveSupport::TestCase

  test "should_have_unique_name_for_survey" do
    test_survey = SurveyRecord.create(:name=>"Survey For Question Test",:message=>"this is a test message",:url=>"test_survey_question",:description=>"abcd")
    test_survey.save!
    
    test_question = Question.create(:survey_record_id=>test_survey.id,:name=>"myName",:record_order=>1)
    test_question.save!
    
    test_question2 = Question.create(:survey_record_id=>test_survey.id,:name=>test_question.name,:record_order=>2)
    assert !test_question2.save , " Question saved with duplicate name"
    
  end
  
  test "should have unique order for survey" do
    
    test_survey = SurveyRecord.create(:name=>"Survey For Question Test",:message=>"this is a test message",:url=>"test_survey_question",:description=>"abcd")
    test_survey.save!
    
    test_question = Question.create(:survey_record_id=>test_survey.id,:name=>"myName",:record_order=>1)
    test_question.save!
    
    test_question2 = Question.create(:survey_record_id=>test_survey.id,:name=>test_question.name,:record_order=>2)
    assert !test_question2.save , " Question saved with duplicate record order "
  end
  
  
  
end
