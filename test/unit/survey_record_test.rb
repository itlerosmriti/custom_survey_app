require 'test_helper'

class SurveyRecordTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  test "should not save without name" do
    survey_record = SurveyRecord.new
    assert !survey_record.save ,"Record Save without Name provided"
  end
  
  test "should have unique url" do 
    survey_record = SurveyRecord.create(:name => 'test_survey',:message =>"Test message",:description=>"test description",:url=>"test_survey")
    assert survey_record.valid?, "Survey was not valid #{survey_record.errors.inspect}"
    
    surveyRec2 = SurveyRecord.create(:name => survey_record.name,:message =>"Test message",:description=>"test description",:url=>"test_survey")
    
    assert !surveyRec2.valid? , " REcord saved with duplicate url"
  end
  
  test "should have unique name" do 
    survey_record = SurveyRecord.create(:name => 'test_survey',:message =>"Test message",:description=>"test description",:url=>"test_survey")
    assert survey_record.valid?, "Survey was not valid #{survey_record.errors.inspect}"
    
    surveyRec2 = SurveyRecord.create(:name => survey_record.name,:message =>"Test message",:description=>"test description",:url=>"new test_survey")
    
    assert !surveyRec2.valid? , " REcord saved with duplicate name"
  end
  
  test "should not save without description" do
    survey_record = SurveyRecord.create(:name=>"abcd")
    assert !survey_record.save, " Record saved without description "
  end
  
  test "should not save without url" do
    survey_record = SurveyRecord.create(:name=>"abcd",:description=>"new description")
    assert !survey_record.save, "Record Saved without url"
  end
  
  test "should not save without message" do
    survey_record = SurveyRecord.new.create(:name=>"abcd",:description=>"new description",:url=>"New URL")
    assert_present !survey_record.message, "Record saved without message"
  end

  
end
